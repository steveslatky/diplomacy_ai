from classes.unit import Unit
from random import randint


class Army(Unit):
    def __init__(self):
        self.type = "army"

    def supply(self, board):
        pass

    def convoy(self, board):
        pass

    # Army units can only move on land.
    def _find_moves(self, board):
        moves = []
        # need to do a check to see if current loc is land or sea
        for p in board.provinces[self.location].borders:
            if not board.provinces[p].is_land:
                continue
            moves.append(p)
        return moves
