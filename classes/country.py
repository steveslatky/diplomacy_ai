# The information holding the "players" information


class Country:
    def __init__(self):
        self.units = []
        self.provinces_held = []
        self.id = None

    def __str__(self):
        return "%s holds %s and has units %s" % (self.id, self.provinces_held, self.units)
