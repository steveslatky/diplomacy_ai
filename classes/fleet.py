from classes.unit import Unit


class Fleet(Unit):

    def __init__(self):
        self.type = "fleet"

    def supply(self, board):
        pass

    def convoy(self, board):
        pass

    def _find_moves(self, board):
        moves = []
        # need to do a check to see if current loc is land or sea
        for p in board.provinces[self.location].borders:
            if not board.provinces[p].is_coastal:
                continue
            # Handle special cases of fleets.
            if self.location == "ank" and p == "smy":
                continue
            moves.append(p)
        return moves
