import pydot

def draw_map(map):
    with open("./output/1.dot", "w") as f:
        header = ("digraph G {\n" +
                  "nodesep=1.5;  center=true;\n" +
                  "node [color=black, shape=rectangle, style=filled, fillcolor= skyblue fixedsize=false, width=0.6];\n")
        f.write(header)
        lines = []
        for prov in map.provinces:
            lines.append(prov + ";")
        lines.append("\n")

        for prov in map.provinces:
            lines.append(prov + "--{" + ','.join(map.provinces[prov].borders) + "};\n")

            # for border in map.provinces[prov].borders:
                # lines.append("%s->%s" % (prov, border))

        f.writelines(lines)
        f.write("}")


def pydot_test():
    x = pydot
    x.Dot

