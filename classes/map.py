from classes.provinces import Province


# Gets the land type and starting country
#
# Key
# x	Neutral Supply Centre
# l	A Land Province
# w	A Sea Province
def _interrupt_start_info(start_info):
    has_fleet = False
    is_occupy = False
    # Empty land
    if start_info is "l":
        owner = None
        is_land = True
        supply = False
    # Water tile
    elif start_info is "w":
        owner = None
        is_land = False
        supply = False
    # Neutral Supply Crate
    elif start_info is "x":
        owner = None
        is_land = True
        supply = True
    else:
        owner = start_info
        if len(start_info) > 1:
            has_fleet = True
            owner = owner[0]
        is_land = True
        supply = True
        is_occupy = True

    return is_land, owner, supply, has_fleet, is_occupy


# Expect border to be array of str abv separated by colan (:)
def _border_to_array(border):
    return border.split(":")


# The board game of diplomacy.
# Just hold a graph view of the provinces
class Map:
    def __init__(self, game_mode):
        self.countries = []  # Index of Ids for owners
        self.provinces = {}
        if game_mode:
            self.get_map_from_csv(game_mode)

    def get_map_from_csv(self, path):
        with open(path, "r") as f:
            for line in f:
                # Extract data from CSV
                split = line.strip().split(",")
                name = split[0].strip()
                start_info = split[1].strip()
                abv = split[2].strip()

                # start info states to country an type of land
                is_land, owner, supply_crate, has_fleet, is_occupy = _interrupt_start_info(start_info)

                # Make the name of the country not matter, give them an id
                if owner is not None:
                    if owner not in self.countries:
                        self.countries.append(owner)

                border = split[3]
                borders = _border_to_array(border.strip())

                is_coastal = split[4].strip() is "c"

                # Add province to the game board
                p = Province(name, abv, owner, is_land, supply_crate, has_fleet, is_occupy, is_coastal, borders)
                self.provinces[abv] = p
        self._verify_map()

    def _verify_map(self):
        _pass = True
        for i in self.provinces:
            for x in self.provinces[i].borders:
                try:
                    self.provinces[x]
                except KeyError:
                    print("Issue with %s borders, %s not found" % (i, x))
                    _pass = False

                if len(x) is not 3:
                    print("There is an issue with %s borders, %s too short" % i)
                    _pass = False
        if not _pass:
            exit("The CSV files has some issue. Can not proceed")

