class Province:
    def __init__(self, name, abv, owner, is_land, supply_crate, has_fleet, is_occupied, is_coastal, borders):
        self.is_land = is_land
        self.is_occupied = is_occupied
        self.name = name
        self.abv = abv
        self.owner = owner
        self.borders = borders  # Array of provinces
        self.has_supply_crate = supply_crate
        self.start_with_fleet = has_fleet
        self.is_coastal = is_coastal

    def __str__(self):
        return "%s - %s %s" % (self.abv, self.owner, self.borders)
