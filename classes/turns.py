def resolve(countries):
    all_units = []
    for c in countries:
        all_units.extend(c.units)
    _resolve(all_units)


def _resolve(all_units):
    for unit in all_units:
        same_loc = [u for u in all_units if u.location == unit.location ]

        # Check if the only item with the same location is it's self.
        if len(same_loc) is 1:
            continue
        for u in same_loc:
            u.location = u.previous_loc

class Turn:
    def summer(self):
        pass
    def winter(self):
        pass
    def spring(self):
        pass
    def retreat(self):
        pass
    def build(self):
        pass

    # Takes in all unites in array.
    # def resolve(self, countries):
    #     for country in countries:
    #         for unit in country.units:


