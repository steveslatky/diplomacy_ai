from abc import ABC, abstractmethod, ABCMeta
from random import randint


class Unit(ABC):
    __metaclass__ = ABCMeta

    def __init__(self):
        self.location = None
        self.type = None
        self.owner = None
        self.previous_loc = None

    def move(self, board):
        possible_moves = self._find_moves(board)
        # pick random move.
        self.location = possible_moves[randint(0, len(possible_moves) - 1)]

    # The unit stays in place. Nothing really happens.
    def hold(self):
        self.previous_loc = self.location
        return self.location

    @abstractmethod
    def supply(self, board):
        pass

    @abstractmethod
    def convoy(self, board):
        pass

    @abstractmethod
    def _find_moves(self, board):
        pass

    def update_location(self, loc):
        self.location = loc

    def resolve_conflict(self):
        self.location = self.previous_loc

    def __str__(self):
        return "%s's %s at %s" % (self.owner, self.type, self.location)
