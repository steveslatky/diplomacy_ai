from rules import *
from classes.turns import *
from classes.graph import *


def main():
    game = Rules()
    game.set_rules(GameMode.EU_STD)
    print(game.map.countries)
    print(game.map.provinces["edi"].start_with_fleet)
    print(game.map.provinces["mun"].start_with_fleet)

    print(game.map.provinces["edi"].is_coastal)
    print(game.map.provinces["mun"].is_coastal)

    print(game.countries[0].units[0].location)
    print(game.countries[0].units[0].type)
    game.countries[0].units[0].move(game.map)
    print(game.countries[0].units[0].location)
    resolve(game.countries)
    print(game.countries[0].units[0].location)

    draw_map(game.map)

if __name__ == "__main__":
    main()
