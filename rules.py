from enum import Enum
from classes.map import Map
from classes.country import Country
from classes.army import Army
from classes.fleet import Fleet

# Ground work to get different maps and rules.
class GameMode(Enum):
    EU_STD = "./data/eu_std.csv"
    EU_VAR = None


class Rules:
    def __init__(self):
        self.winning_provinces = None
        self.num_start_countries = None
        self.map = None
        self.countries = []

    def set_rules(self, mode):
        if mode == GameMode.EU_STD:
            self.winning_provinces = 18
            self.num_start_countries = 7
            self.map = Map(GameMode.EU_STD.value)

        self._set_up()

    def _set_up(self):
        for player in self.map.countries:
            country = Country()
            country.id = player
            for p in self.map.provinces:
                if self.map.provinces[p].owner is player:
                    if self.map.provinces[p].start_with_fleet:
                        unit = Fleet()
                    else:
                        unit = Army()
                    unit.owner = player
                    unit.location = p
                    unit.previous_loc = p
                    country.units.append(unit)
                    country.provinces_held.append(p)
            self.countries.append(country)
